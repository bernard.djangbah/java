import account.Account;
import org.testng.annotations.Test;
import trades.BondTrade;

import static org.junit.jupiter.api.Assertions.*;

class AccountTest {
    @Test
    void AccountTest() {
        double price = 12.0;
        int quantity = 3;
        Account account = new Account();
        double value = account.getValue();
        BondTrade trade = new BondTrade("ID", "SYM", quantity, price, 13);
        account.addTrade(trade);
        assertEquals(value + (price * quantity), account.getValue());
    }
}
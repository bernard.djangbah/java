import client.Client;
import memberships.Bronze;
import org.junit.jupiter.api.Test;
import trades.BondTrade;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class ClientTest {
    @Test
    public void clientFirstTradeBronzeTest () {
        Client client = new Client("Bernard", "Tetteh");
        BondTrade trade = new BondTrade("ID", "SYM", 3, 15, 16);
        Bronze bronze = new Bronze();
        bronze.setPoints();
        bronze.setPoints();
        client.addTrade(trade);
        client.addTrade(trade);
        System.out.println(client);
        assertEquals(bronze, client.getMembership());
    }
}

import org.junit.jupiter.api.Test;
import trades.BondTrade;

import static org.junit.jupiter.api.Assertions.*;

class TradeTest {

    @Test
    void setPrice() {
        BondTrade trade = new BondTrade("ID", "SYM", 3, 15);
        trade.setPrice(-20);
        assertTrue(trade.getPrice() >= 0);
    }
}
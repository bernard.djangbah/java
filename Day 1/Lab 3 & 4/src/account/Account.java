package account;

import trades.Trade;

public class Account {
    private double value;

    public void setValue(double value) {
        this.value = value;
    }

    public void addTrade (Trade trade) {
        this.value += (trade.getPrice() * trade.getQuantity());
    }

    public double getValue() {
        return this.value;
    }
}

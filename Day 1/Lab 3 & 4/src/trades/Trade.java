package trades;

import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public abstract class Trade {
    private String id;
    private String symbol;
    private int quantity;
    private double price;
    private LocalDate tDate;
    private LocalTime tTime;

    public Trade (String id, String symbol, int quantity, double price) {
        this.id = id;
        this.symbol = symbol;
        this.quantity = quantity;
        this.price = price;

        tDate = LocalDate.now();
        tTime = LocalTime.now();;
    }

    public Trade (String id, String symbol, int quantity) {
        this.id = id;
        this.symbol = symbol;
        this.quantity = quantity;
    }

    public abstract void calcDividend();

    public void setPrice(double price) {
        if (price > 0)
            this.price = price;
    }

    public double getPrice(){
        return this.price;
    }

    public int getQuantity(){
        return this.quantity;
    }

    public LocalDate gettDate() {
        return tDate;
    }

    @Override
    public String toString() {
        return "trades.Trade{" +
                "id='" + id + '\'' +
                ", symbol='" + symbol + '\'' +
                ", quantity=" + quantity +
                ", price=" + price +
                ", tDate=" + tDate +
                ", tTime=" + tTime +
                '}';
    }
}

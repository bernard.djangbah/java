package trades;

public class FundTrade extends Trade {
    private double dividendPercentage;

    public FundTrade (String id, String symbol, int quantity, double price, double dividendPercentag) {
        super(id, symbol, quantity, price);
        this.dividendPercentage = dividendPercentage;
    }

    public FundTrade (String id, String symbol, int quantity, double dividendPercentage) {
        super(id, symbol, quantity);
        this.dividendPercentage = dividendPercentage;
    }

    @Override
    public void calcDividend() {

    }
}

package client;

import account.Account;
import memberships.Bronze;
import memberships.Gold;
import memberships.MembershipType;
import memberships.Silver;
import trades.Trade;

import java.util.Arrays;

public class Client {
    private String firstName;
    private String lastName;
    private MembershipType membership;
    private Trade trades[] = new Trade[10];
    private Account account;

    public Client (String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        account = new Account();
    }

    public void addTrade (Trade trade) {
        if (membership == null) {
            membership = new Bronze();
        }
        if (membership.canTrade(trades, trade)) {
            account.addTrade(trade);

            if (isFull()) {
                int position = trades.length;
                growArray();
                trades[position] = trade;
            } else {
                int i = 0;
                while (trades[i] != null)
                    i++;
                trades[i] = trade;
            }
            if (membership.getPoints() == 9) {
                membership = new Silver(10);
                return;
            } else if (membership.getPoints() == 19) {
                membership = new Gold(20);
                return;
            }

            membership.setPoints();
        } else {
            if (!hasTrade()){
                membership = null;
            }
            System.out.println("Sorry, you can't make a trade at this time.");
        }
    }

    public MembershipType getMembership() {
        return membership;
    }

    private boolean isFull() {
        return trades[trades.length - 1] != null;
    }

    private boolean hasTrade() {
        return trades[0] != null;
    }

    private void growArray() {
            Trade[] newArray = new Trade[trades.length * 2];

            System.arraycopy(trades, 0, newArray, 0, trades.length);

            trades = newArray;
    }

    @Override
    public String toString() {
        return "client.Client{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", membership=" + membership +
                ", trades=" + Arrays.toString(trades) +
                ", account=" + account +
                '}';
    }
}

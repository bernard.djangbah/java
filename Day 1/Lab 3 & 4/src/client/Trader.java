package client;

import account.Account;
import trades.Trade;

public class Trader {
    private String name;
    private Account account;

    public Trader(String name){
        this.name = name;
        account = new Account();
    }

    public void addTrade (Trade trade) {
        account.addTrade(trade);
    }
}

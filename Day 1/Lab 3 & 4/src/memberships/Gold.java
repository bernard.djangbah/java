package memberships;

public class Gold extends MembershipType {
    public Gold (int points) {
        this.points = points;
        maxTrade = 20;
    }

    @Override
    public String toString() {
        return "Gold{" +
                "points=" + points +
                ", maxTrade=" + maxTrade +
                '}';
    }
}

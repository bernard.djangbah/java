package memberships;

import trades.Trade;

import java.time.LocalTime;

public class Bronze extends MembershipType {
    private int tradeTime = 10;

    public Bronze() {
        this.points = 0;
        maxTrade = 5;
    }

    @Override
    public boolean canTrade(Trade[] trades, Trade currentTrade) {
        LocalTime time = LocalTime.now();
        int hour = time.getHour();

        if (hour < tradeTime) {
            return false;
        }

        if (!super.canTrade(trades, currentTrade)){
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return "Bronze{" +
                "tradeTime=" + tradeTime +
                ", points=" + points +
                ", maxTrade=" + maxTrade +
                '}';
    }
}

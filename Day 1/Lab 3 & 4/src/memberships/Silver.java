package memberships;

public class Silver extends MembershipType {
    public Silver (int points) {
        this.points = points;
        maxTrade = 10;
    }

    @Override
    public String toString() {
        return "Silver{" +
                "points=" + points +
                ", maxTrade=" + maxTrade +
                '}';
    }
}

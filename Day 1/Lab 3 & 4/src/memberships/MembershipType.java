package memberships;

import trades.Trade;

import java.time.LocalDate;
import java.util.Objects;

public abstract class MembershipType {
    protected int points;

    private int maxTradeAmount = 10000;
    protected int maxTrade;

    public void setPoints() {
        ++this.points;
    }

    public int getPoints() {
        return points;
    }

    protected boolean maxTradeExceeded(Trade[] trades, Trade currentTrade) {
        int totalVal = 0;
        int i = 0;
        while (i < trades.length && trades[i] != null){
            totalVal += trades[i].getPrice();
            i++;
        }

        if (totalVal + currentTrade.getPrice() < maxTradeAmount)
            return false;
        return true;
    }

    public boolean canTrade(Trade[] trades, Trade currentTrade) {
        if (maxTradeExceeded(trades, currentTrade)){
            return false;
        }
        LocalDate date = LocalDate.now();

        int numOfTradesToday = 0;
        int i = 0;
        while (i < trades.length && trades[i] != null) {
            if (trades[i].gettDate().equals(date))
                numOfTradesToday++;
            i++;
        }

        if (numOfTradesToday >= maxTrade){
            return false;
        }

        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MembershipType that = (MembershipType) o;
        return points == that.points;
    }

    @Override
    public int hashCode() {
        return Objects.hash(points);
    }
}

import client.Client;
import trades.BondTrade;
import trades.Trade;

public class main {
    public static void main(String[] args) {
        Client bernard = new Client("bernard", "tetteh");
        Trade trade1 = new BondTrade("1", "BTC", 3, 5, 20);
        Trade trade2 = new BondTrade("2", "BTC", 3, 5, 20);
        Trade trade3 = new BondTrade("3", "BTC", 3, 5, 20);
        Trade trade4 = new BondTrade("4", "BTC", 3, 5, 20);
        Trade trade5 = new BondTrade("5", "BTC", 3, 5, 20);
        Trade trade6 = new BondTrade("6", "BTC", 3, 5, 20);
        bernard.addTrade(trade1);
        bernard.addTrade(trade2);
        bernard.addTrade(trade3);
        bernard.addTrade(trade4);
        bernard.addTrade(trade5);
        bernard.addTrade(trade6);


        System.out.println(bernard);
    }
}

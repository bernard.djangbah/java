public class main {
    public static void main(String[] args) {
        int random = (int) (Math.random() * 100) + 1;

        if (random % 2 == 0)
            System.out.println(random + " is even.");
        else
            System.out.println(random + " is odd.");

        if (random == 0){
            System.out.println("frozen");
        } else if (random > 0 && random < 15) {
            System.out.println("cold");
        } else if (random > 14 && random < 25) {
            System.out.println("cool");
        } else {
            System.out.println("blah blah blah");
        }
    }
}
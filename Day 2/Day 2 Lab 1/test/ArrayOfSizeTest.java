import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArrayOfSizeTest {
    @Test
    public void test(){
        ArrayOfSize array = new ArrayOfSize();
        int[] arrayTest = {0,1,2,3,4,5,6,7,8,9};
        assertArrayEquals(arrayTest, array.array);
    }
}
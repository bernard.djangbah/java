import java.util.Arrays;
import java.util.stream.IntStream;

public class ArrayOfSix {
    private int[] array;

    public ArrayOfSix() {
        array = new int[6];
        int num;
        for (int i = 0; i < array.length; i++) {
            num = (int) (Math.random() * 49) + 1;

            boolean inserted = false;

            while (!inserted) {
                if (isContained(num)) {
                    num = (int) (Math.random() * 49) + 1;
                } else {
                    array[i] = num;
                    inserted = true;
                }
            }
        }

        Arrays.sort(array);
    }

    private boolean isContained(int num) {
        for (int i = 0; i < array.length; i++){
            if (array[i] == num)
                return true;
        }
        return false;
    }

    public void print() {
        for (int i = 0; i < array.length; i++)
            System.out.println(array[i]);
    }
}

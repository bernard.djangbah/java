import java.util.Comparator;

public class SortBySuit implements Comparator<Card> {
    @Override
    public int compare(Card card1, Card card2) {
        return card1.getSuit().getTypeNum() - card2.getSuit().getTypeNum();
    }
}

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Dealer {
    private List<Card> deck;
    private Table table;

    private int size;

    public Dealer (List<Player> players) {
        deck = new ArrayList<>();

        for (Suit suit : Suit.values()){
            for (Value value : Value.values()){
                deck.add(new Card(suit, value));
            }
        }

        if (players.size() >= 5)
            size = 5;
        else size = players.size();

        table = new Table(players, size);
    }

    public void shuffle() {
        Collections.shuffle(deck);
    }

    public List<Card> getDeck() {
        return deck;
    }

    public void dealOneCard () {
        for (int i = 0; i < size; i++){
            table.getSeats()[i] = table.getPlayers().get(i);
        }
        for (int i = 0; i < size; i++){
            table.getSeats()[i].addtoCards(deck.get(i));
        }
    }

    public Table getTable() {
        return table;
    }
}

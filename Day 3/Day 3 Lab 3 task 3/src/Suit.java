public enum Suit {
    S(4),
    H(3),
    D(2),
    C(1);

    private int typeNum;
    private Suit(int n) {
        typeNum = n;
    }

    public int getTypeNum() {
        return typeNum;
    }
}

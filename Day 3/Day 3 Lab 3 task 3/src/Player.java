import java.util.ArrayList;
import java.util.List;

public class Player {
    private String name;
    private List<Card> handOfCards;

    public Player (String name){
        this.name = name;
        handOfCards = new ArrayList<>();
    }

    public void addtoCards(Card card) {
        handOfCards.add(card);
    }

    public String getName() {
        return name;
    }

    public List<Card> getHandOfCards() {
        return handOfCards;
    }

    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                ", handOfCards=" + handOfCards +
                '}';
    }
}

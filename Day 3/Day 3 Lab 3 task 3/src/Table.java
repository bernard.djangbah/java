import java.util.Arrays;
import java.util.List;

public class Table {
    private List<Player> players;
    private Player[] seats;

    public Table (List<Player> players, int size) {
        this.players = players;
         seats = new Player[size];
    }

    public List<Player> getPlayers() {
        return players;
    }

    public Player[] getSeats() {
        return seats;
    }

    @Override
    public String toString() {
        return "Table{" +
                "players=" + players +
                ", seats=" + Arrays.toString(seats) +
                '}';
    }
}

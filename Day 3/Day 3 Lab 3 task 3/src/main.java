import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class main {
    public static void main(String[] args) {

//        TreeSet<Card> hand = new TreeSet<>();

//        for (Suit suit : Suit.values()){
//            for (Value value : Value.values()){
//                hand.add(new Card(suit, value));
//            }
//        }

        Card c6 = new Card(Suit.C, Value.SIX);
        Card c5 = new Card(Suit.H, Value.FIVE);
        Card s9 = new Card(Suit.S, Value.NINE);
        Card d11 = new Card(Suit.C, Value.JACK);
        Card h14 = new Card(Suit.C, Value.KING);

//        List<Card> list = new ArrayList<>();
//        list.add(c6);
//        list.add(c5);
//        list.add(s9);
//        list.add(d11);
//        list.add(h14);

//        Collections.sort(list, new SortByValue());
//        for (Card card : list)
//            System.out.println(card.getSuit() + " of " + card.getValue());

//        Dealer dealer = new Dealer();

//        dealer.shuffle();

//        for (Card card : dealer.getDeck())
//            System.out.println(card.getSuit() + " of " + card.getValue());


//        for (Card card : hand)
//            System.out.println(card.getSuit() + " of " + card.getValue());

        Player p1 = new Player("Player 1");
        Player p2 = new Player("Player 2");
        Player p3 = new Player("Player 3");
//        Player p4 = new Player("Player 4");
//        Player p5 = new Player("Player 5");
//        Player p6 = new Player("Player 6");

        List<Player> playerList = new ArrayList<>();
        playerList.add(p1);
        playerList.add(p2);
        playerList.add(p3);
//        playerList.add(p4);
//        playerList.add(p5);
//        playerList.add(p6);

        Dealer dealer = new Dealer(playerList);

        dealer.shuffle();

        dealer.dealOneCard();

        System.out.println(dealer.getTable());
    }
}

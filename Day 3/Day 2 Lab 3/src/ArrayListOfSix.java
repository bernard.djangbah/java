import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;

public class ArrayListOfSix {
    private ArrayList<Integer> list;

    private int size = 6;

    public ArrayListOfSix () {
        list = new ArrayList<>();

        int num;
        for (int i = 0; i < size; i++) {
            num = (int) (Math.random() * 49) + 1;

            boolean inserted = false;

            while (!inserted) {
                if (isContained(num)) {
                    num = (int) (Math.random() * 49) + 1;
                } else {
                    list.add(num);
                    inserted = true;
                }
            }
        }
        Collections.sort(list);
    }

    private boolean isContained(int num) {
        for (int i = 0; i < list.size(); i++){
            if (list.get(i) == num)
                return true;
        }
        return false;
    }

    public void print() {
        for (Integer i : list) {
            System.out.println(i);
        }
    }
}
